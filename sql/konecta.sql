-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql
-- Tiempo de generación: 10-08-2022 a las 13:21:46
-- Versión del servidor: 5.6.51
-- Versión de PHP: 8.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `konecta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adviser`
--

CREATE TABLE `adviser` (
  `asesor_id` int(15) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cedula` int(30) NOT NULL,
  `telefono` int(30) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `genero` enum('Masculino','Femenino') NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `sede_id` int(11) NOT NULL,
  `usuario_id` int(15) NOT NULL,
  `edad` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adviser`
--

INSERT INTO `adviser` (`asesor_id`, `nombre`, `cedula`, `telefono`, `fecha_nacimiento`, `genero`, `cliente_id`, `sede_id`, `usuario_id`, `edad`, `fecha_creacion`) VALUES
(1, 'Juan Carlos Villar herrera', 1082889, 2147483647, '2022-08-10', 'Masculino', 2, 1, 1, 0, '2022-08-10 08:12:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `cliente_id` int(11) NOT NULL,
  `cliente` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`cliente_id`, `cliente`) VALUES
(1, 'Cliente 1'),
(2, 'Cliente 2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sedes`
--

CREATE TABLE `sedes` (
  `sede_id` int(15) NOT NULL,
  `sede` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sedes`
--

INSERT INTO `sedes` (`sede_id`, `sede`) VALUES
(1, 'Ruta N'),
(2, 'Puerto seco'),
(3, 'Buro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id_usuario` int(15) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `contrasena` varchar(100) NOT NULL,
  `estado` enum('activo','inactivo') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id_usuario`, `usuario`, `contrasena`, `estado`) VALUES
(1, 'comunicaciones', 'Reg1234', 'activo'),
(2, 'gestión', 'Ges1234', 'activo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adviser`
--
ALTER TABLE `adviser`
  ADD PRIMARY KEY (`asesor_id`),
  ADD KEY `indexcliente` (`cliente_id`),
  ADD KEY `indexsede` (`sede_id`),
  ADD KEY `indexusuario` (`usuario_id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`cliente_id`);

--
-- Indices de la tabla `sedes`
--
ALTER TABLE `sedes`
  ADD PRIMARY KEY (`sede_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `adviser`
--
ALTER TABLE `adviser`
  MODIFY `asesor_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `cliente_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sedes`
--
ALTER TABLE `sedes`
  MODIFY `sede_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id_usuario` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `adviser`
--
ALTER TABLE `adviser`
  ADD CONSTRAINT `indexcliente` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`cliente_id`),
  ADD CONSTRAINT `indexsede` FOREIGN KEY (`sede_id`) REFERENCES `sedes` (`sede_id`),
  ADD CONSTRAINT `indexusuario` FOREIGN KEY (`usuario_id`) REFERENCES `user` (`id_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
