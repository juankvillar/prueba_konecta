<!DOCTYPE html>
<html lang="es">
<head>
  <title>Prueba Konecta</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../lib/bootstrap.min.css">
  <link rel="icon" type="image/png" sizes="32x32" href="../resource/img/favicon.png">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<?php 
@session_start();
if( isset($_SESSION['id_usuario']) ){
  echo '<strong> <a style="color:red; float:right;border: 1px solid red;margin:2%;"  href="controllers/logout.php">Salir del sistema</a> </strong>';
}
?>
  