<?php include('controllers/seguridad.php'); ?>
<?php include('controllers/asesores.php'); ?>
<?php include('../template/header.php'); ?>
<div class="container">
  <h1>Formulario para editar asesor</h1>
  <div class="alert alert-success" role="alert">Todos los campos son obligatorios</div>
  <form class="form-horizontal" id="frm">
    <div class="form-group">
      <label for="nombre" class="col-sm-2 control-label">Nombre completo *</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre completo" required value="<?php echo $asesor->nombre ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="cedula" class="col-sm-2 control-label">Cédula *</label>
      <div class="col-sm-10">
        <input type="number" min="0" class="form-control" id="cedula" name="cedula" placeholder="Cédula" required value="<?php echo $asesor->cedula ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="telefono" class="col-sm-2 control-label">Teléfono *</label>
      <div class="col-sm-10">
        <input type="number" min="0" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" required value="<?php echo $asesor->telefono ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="fecha_nacimiento" class="col-sm-2 control-label">Fecha de nacimiento *</label>
      <div class="col-sm-10">
        <input type="date" min="0" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Teléfono" required value="<?php echo $asesor->fecha_nacimiento ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="genero" class="col-sm-2 control-label">Genero *</label>
      <div class="col-sm-10">
        <select class="form-control" name="genero" id="genero" required style="width: 100%;" required>
          <option value="">Seleccionar</option>
          <option value="Masculino" <?php echo $asesor->genero == 'Masculino'? 'selected' : '' ;  ?>>Masculino</option>
          <option value="Femenino"  <?php echo $asesor->genero == 'Femenino'? 'selected' : '';  ?>>Femenino</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="cliente_id" class="col-sm-2 control-label">Cliente *</label>
      <div class="col-sm-10">
        <select class="form-control" name="cliente_id" id="cliente_id" required style="width: 100%;" required>
          <option value="">Seleccionar</option>
          <?php foreach ($clientes as $key => $value) { ?>
            <option value="<?php echo $value->cliente_id ?>" <?php echo $asesor->sede_id ==  $value->cliente_id ? 'selected' : '' ;  ?> ><?php echo $value->cliente ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="sede_id" class="col-sm-2 control-label">Sede *</label>
      <div class="col-sm-10">
        <select class="form-control" name="sede_id" id="sede_id" required style="width: 100%;" required>
          <option value="">Seleccionar</option>
          <?php foreach ($sedes as $key => $value) { ?>
            <option value="<?php echo $value->sede_id ?>" <?php echo $asesor->sede_id ==  $value->sede_id ? 'selected' : '' ;  ?> ><?php echo $value->sede ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Guardar</button>
        <a class="btn btn-warning" href="index.php">Volver</a>
      </div>
    </div>
  </form>

</div>
<?php include('../template/footer.php'); ?>
<script>
  let opcn = "editar"
  let asesor_id = <?php echo $_GET['id']?>;
</script>
<script src="js/asesores.js?sin_cache=<?php echo md5(time()); ?>"></script>