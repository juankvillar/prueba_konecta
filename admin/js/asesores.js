$('#frm').submit(function (e) { 
    e.preventDefault();
    let datos = $( this ).serializeArray();
        datos.push({name: 'opcn' , value: opcn })
        datos.push({name: 'asesor_id' , value: asesor_id })

    $.ajax({
        type: 'post',
        url: 'controllers/asesores.php',
        dataType: 'json',
        data: datos,
    }).done(function (data) {
        swal(data.msj,'',data.type);
    })
    .fail(function (data) {
        swal('No se realizó ningún cambio','','error');
    })
});

$("body").on("click", ".swal-button--confirm", function (e) {
    location.reload();
  });

  $("body").on("click", ".swal-overlay", function (e) {
    location.reload();
  });


  $('.eliminar').click(function (e) { 
      e.preventDefault();

      if( !confirm('¿Esta seguro de eliminar este asesor?')){ return false;}
      let id = $( this ).data('id')
      $.ajax({
        type: 'post',
        url: 'controllers/asesores.php',
        dataType: 'json',
        data: {opcn: "eliminar", id:id},
    }).done(function (data) {
        alert(data.msj)
        if( !data.error){
            location.reload();
        }
        
    })
    .fail(function (data) {
        swal('No se realizó ningún cambio','','error');
    })
  });