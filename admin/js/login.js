$('#frm').submit(function (e) { 
    e.preventDefault();
    let datos = $( this ).serializeArray();
        datos.push({name: 'opcn' , value: 'loguearse' })

    $.ajax({
        type: 'post',
        url: 'controllers/login.php',
        dataType: 'json',
        data: datos,
    }).done(function (data) {
        
        if( !data.error ){
            location.reload();
        }else{
            swal(data.msj,'',data.type);
        }
    })
    .fail(function (data) {
        swal('No se realizó ningún cambio','','error');
    })
});

// $("body").on("click", ".swal-button--confirm", function (e) {
//     location.reload();
//   });


//   $('.eliminar').click(function (e) { 
//       e.preventDefault();

//       if( !confirm('¿Esta seguro de eliminar este empleado?')){ return false;}
//       let id = $( this ).data('id')
//       $.ajax({
//         type: 'post',
//         url: 'controllers/empleados.php',
//         dataType: 'json',
//         data: {opcn: "eliminar", id:id},
//     }).done(function (data) {
//         swal(data.msj,'',data.type);
//     })
//     .fail(function (data) {
//         swal('No se realizó ningún cambio','','error');
//     })
//   });