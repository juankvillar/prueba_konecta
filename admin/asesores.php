<?php include('controllers/seguridad.php'); ?>
<?php include('controllers/asesores.php'); ?>
<?php include('../template/header.php'); ?>
<div class="container">
  <h1>Formulario para crear asesor</h1>
  <div class="alert alert-success" role="alert">Todos los campos son obligatorios</div>
  <form class="form-horizontal" id="frm">
    <div class="form-group">
      <label for="nombre" class="col-sm-2 control-label">Nombre completo *</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre completo" required>
      </div>
    </div>

    <div class="form-group">
      <label for="cedula" class="col-sm-2 control-label">Cédula *</label>
      <div class="col-sm-10">
        <input type="number" min="0" class="form-control" id="cedula" name="cedula" placeholder="Cédula" required>
      </div>
    </div>

    <div class="form-group">
      <label for="telefono" class="col-sm-2 control-label">Teléfono *</label>
      <div class="col-sm-10">
        <input type="number" min="0" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" required>
      </div>
    </div>

    <div class="form-group">
      <label for="fecha_nacimiento" class="col-sm-2 control-label">Fecha de nacimiento *</label>
      <div class="col-sm-10">
        <input type="date" min="0" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Teléfono" required>
      </div>
    </div>

    <div class="form-group">
      <label for="genero" class="col-sm-2 control-label">Genero *</label>
      <div class="col-sm-10">
        <select class="form-control" name="genero" id="genero" required style="width: 100%;" required>
          <option value="">Seleccionar</option>
          <option value="Masculino">Masculino</option>
          <option value="Femenino">Femenino</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="cliente_id" class="col-sm-2 control-label">Cliente *</label>
      <div class="col-sm-10">
        <select class="form-control" name="cliente_id" id="cliente_id" required style="width: 100%;" required>
          <option value="">Seleccionar</option>
          <?php foreach ($clientes as $key => $value) { ?>
            <option value="<?php echo $value->cliente_id ?>"><?php echo $value->cliente ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="sede_id" class="col-sm-2 control-label">Sede *</label>
      <div class="col-sm-10">
        <select class="form-control" name="sede_id" id="sede_id" required style="width: 100%;" required>
          <option value="">Seleccionar</option>
          <?php foreach ($sedes as $key => $value) { ?>
            <option value="<?php echo $value->sede_id ?>"><?php echo $value->sede ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Guardar</button>
      </div>
    </div>
  </form>
  <!-- Fin formulario crear empleado -->
  <hr>
  <!-- Inicio listar empleados -->
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Cedula</th>
        <th>Teléfono</th>
        <th>Fecha nacimiento</th>
        <th>Edad</th>
        <th>Genero</th>
        <th>Cliente</th>
        <th>Sede</th>
        <th>Creador</th>
        <th>Fecha creación</th>
        <th>Modificar</th>
        <th>Eliminar</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($asesores as $key => $value) { ?>
        <tr>
          <td><?php echo $value->nombre ?></td>
          <td><?php echo $value->cedula ?></td>
          <td><?php echo $value->telefono ?></td>
          <td><?php echo $value->fecha_nacimiento ?></td>
          <td><?php echo $value->edad ?></td>
          <td><?php echo $value->genero ?></td>
          <td><?php echo $value->cliente ?></td>
          <td><?php echo $value->sede ?></td>
          <td><?php echo $value->usuario ?></td>
          <td><?php echo $value->fecha_creacion ?></td>
          <td> <a style="color: black;" href="asesores_editar.php?opcn=editar&id=<?php echo $value->asesor_id ?>"><i class="fa fa-pencil-square-o fa-fw"></i> </a> </td>
          <td> <a style="color: black;" href="" class="eliminar" data-id="<?php echo $value->asesor_id ?>"><i class="fa fa-trash fa-fw"></i> </a> </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php include('../template/footer.php'); ?>
<script>
  let opcn = "crear"
  let asesor_id = '';
</script>
<script src="js/asesores.js?sin_cache=<?php echo md5(time()); ?>"></script>