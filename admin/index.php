<?php
@session_start(); 
if( isset($_SESSION['id_usuario'])){ header('Location: asesores.php'); } ?>
<?php include('../template/header.php'); ?>
<div class="container">
  <!-- <div class="alert alert-danger" role="alert">usuario o contraseña incorrecta intente nuevamente</div> -->
  <div style="text-align: center; padding: 4%;">
    <img src="../resource/img/logo.svg" alt="" style="alig">
  </div>
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
      <form class="form-horizontal" id="frm">
        <div class="form-group">
          <label for="usuario" class="col-sm-2 control-label">Usuario</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Ingrese el usuario" required>
          </div>
        </div>

        <div class="form-group">
          <label for="contrasena" class="col-sm-2 control-label">Contraseña</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" id="contrasena" name="contrasena" placeholder="ingrese contraseña" required>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Ingresar</button>
          </div>
        </div>
      </form>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
<?php include('../template/footer.php'); ?>
<script src="js/login.js?sin_cache=<?php echo md5(time()); ?>"></script>