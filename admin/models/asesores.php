<?php


Class Asesores{

	public $con = NULL;
	public $usuario_id = NULL;

	public function __construct($ajax = '') {
		include_once($ajax.'../config/init_db.php');
		$this->con = $mbd;
	}

	public function getAsesores(){	
		$query = "SELECT a.*, c.cliente, s.sede, u.usuario  FROM adviser a
					INNER JOIN clientes c
						on a.cliente_id = c.cliente_id
					INNER JOIN sedes s
						on a.sede_id = s.sede_id
					INNER JOIN user u
						on a.usuario_id = u.id_usuario;";
		$res = $this->con->query($query);
        $asesores = $res->fetchAll();
		return $asesores;
	}

	public function getAsesor( $asesor_id ){
		$query = "SELECT a.* FROM adviser a where asesor_id = $asesor_id;";
		$res = $this->con->query($query);
        $asesor = $res->fetch();
		return $asesor;
	}
	
	public function getClientes(){
		
		$query = "SELECT * FROM clientes";
		$res = $this->con->query($query);
        $asesores = $res->fetchAll();
		return $asesores;
	}

	public function getSedes(){
		$query = "SELECT * FROM sedes";
		$res = $this->con->query($query);
        $asesores = $res->fetchAll();
		return $asesores;
	}

	public function gegetAsesor( $asesor_id ){
		$query = "SELECT * FROM adviser where asesor_id = $asesor_id;";
		$res = $mbd->query($query);
        $asesor = $res->fetch();
		return $asesor;
	}

	public  function insertAsesor( $p ){
		extract($p);
		
		$edad = $this->calcularEdad( $fecha_nacimiento );

		$usuario_id = USUARIO_ID;
		$fecha_creacion	 = FECHA;
		$query = "INSERT INTO adviser
					(
					nombre,
					cedula,
					telefono,
					fecha_nacimiento,
					genero,
					cliente_id,
					sede_id,
					usuario_id,
					edad,
					fecha_creacion
					)
					VALUES
					(
					'$nombre',
					'$cedula',
					'$telefono',
					'$fecha_nacimiento',
					'$genero',
					'$cliente_id',
					'$sede_id',
					'$usuario_id',
					'$edad',
					'$fecha_creacion'
					);";
		$res = $this->con->query($query);
            if ($res) {
                $respuesta['error'] = false;
                $respuesta['msj'] = 'Asesor agregado correctamente';
                $respuesta['type'] = "success";
            } else {
                $respuesta['error'] = true;
                $respuesta['msj'] = 'No se pudo agregar el asesor';
                $respuesta['type'] = "error";
            }
			return $respuesta;
	}
	
	public  function updateAsesor( $p ){
		extract($p);
		$edad = $this->calcularEdad( $fecha_nacimiento );
		$query = "UPDATE adviser
					SET
					nombre 				= '$nombre',
					cedula 				= '$cedula',
					telefono 			= '$telefono',
					fecha_nacimiento 	= '$fecha_nacimiento',
					genero 				= '$genero',
					cliente_id 			= '$cliente_id',
					sede_id 			= '$sede_id',
					edad 				= '$edad'
					WHERE asesor_id 	= $asesor_id;";
		$resultSet_usr = $this->con->exec($query);
            if ($resultSet_usr) {
                $respuesta['error'] = false;
                $respuesta['msj'] = 'Asesor agregado correctamente';
                $respuesta['type'] = "success";
            } else {
                $respuesta['error'] = true;
                $respuesta['msj'] = 'No se realizó ningún cambio';
                $respuesta['type'] = "error";
            }
			return $respuesta;
	}

	public function calcularEdad($fecha_nacimiento)
	{
		$cumpleanos = new DateTime($fecha_nacimiento);
		$hoy 		= new DateTime();
		$annos 		= $hoy->diff($cumpleanos);
		$edad		=  $annos->y;
		return $edad;
	}

	public  function deletedAsesor( $asesor_id ){
		$query = "DELETE FROM adviser WHERE asesor_id = $asesor_id;";
		$resultSet_usr = $this->con->exec($query);
            if ($resultSet_usr) {
                $respuesta['error'] = false;
                $respuesta['msj'] = 'Asesor eliminado correctamente';
                $respuesta['type'] = "success";
            } else {
                $respuesta['error'] = true;
                $respuesta['msj'] = 'No se realizó ningún cambio';
                $respuesta['type'] = "error";
            }
			return $respuesta;
	}

}

